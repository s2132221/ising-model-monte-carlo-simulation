# Set default parameters if N and T are not specified
T?=2.8853901 # Critical temperature of infinite 2D Ising model
N?=50
type?=g


# Require all targets to be updated always (N might always change!).
.PHONY: ising_mc.o bin/animation.o test animation data_generation glauber_data kawasaki_data clean dir

dir:
	mkdir -p bin/

ising_mc.o: src/ising_mc.cc dir
	g++ -o bin/ising_mc.o -c src/ising_mc.cc -Wall -O3 -D NN=$N

bin/animation.o: src/animation.cc dir
	g++ -o bin/animation.o -c src/animation.cc -Wall -O3 -D NN=$N

test: clean src/ising_mc.cc src/test.cc dir
	g++ -o bin/ising_mc.o -c src/ising_mc.cc -Wall -O3 -D NN=$N
	g++ -o test src/test.cc bin/ising_mc.o -Wall -O3 -D NN=$N

animation: gp_scripts/animation.gp dir
	g++ -o bin/ising_mc.o -c src/ising_mc.cc -Wall -O3 -D NN=$N
	g++ -o bin/animation.o -c src/animation.cc -Wall -O3 -D NN=$N
	g++ -o animation bin/animation.o bin/ising_mc.o -O3 -Wall -D NN=$N
	gnuplot -e "N='$N'" gp_scripts/animation.gp  &
	./animation -T $T -t $(type)


data_generation: src/ising_mc.cc src/data_generation.cc src/stat_tools.cc dir
	g++ -o bin/ising_mc.o -c src/ising_mc.cc -Wall -O3 -D NN=$N
	g++ -o bin/stat_tools.o -c src/stat_tools.cc -Wall -O3 -D NN=$N
	g++ -o data_generation src/data_generation.cc bin/ising_mc.o bin/stat_tools.o -Wall -O3 -D NN=$N

glauber_data: data_generation dir
	./data_generation -t g -f data/glauber_data.txt --adaptive -s 987654321

kawasaki_data: data_generation dir
	./data_generation -t k -f data/kawasaki_data.txt -Tmin 2.0 -Tmax 7.0 -Tcrit 4.5 -alpha 2.0 -Nt 51 --adaptive -s 987654321

clean:
	rm -f test
	rm -rf bin/*
	rm -f animation.txt
	rm -f animation.txt.tmp
