#include <algorithm>
#include <cmath>
#include <cstring>
#include <ctime>
#include <random>
#include <string>

#include "ising_mc.h"

using namespace std;

/**
 * Parse a commandline argument.
 *
 * Return the value set for the flag `flag` in the commandline arguments or
 * a default parameter.
 *
 * @param begin The first index of the command line argument pointer.
 * @param end The last index of the command line argument pointer.
 * @param flag The flag to look for.
 * @param def The default value to return if `flag` was not specified.
 */
const char* parse_cmd_option(char** begin, char** end, const string& flag,
                             const char* def) {
    char** itr = std::find(begin, end, flag);
    if (itr != end && ++itr != end) {
        return *itr;
    }
    return def;
}

/**
 * Determine whether a specific flag exists in the command line arguments.
 *
 * @param begin The first index of the command line argument pointer.
 * @param end The last index of the command line argument pointer.
 */
bool cmd_option_exists(char** begin, char** end, const string& flag) {
    return std::find(begin, end, flag) != end;
}

/**
 * Print the possible commandline arguments.
 */
void print_usage() {
    cout << "Usage: ./animation [-f filename] [-Nm N_measurements] "
            "[-T Temperature] [-t (g|k)]"
            "[--help] [-h]"
         << endl;
}

/**
 * Generate the data for an animated plot using Glauber-dynamics.
 *
 * This will create a file named `animation.txt` which can be used with gnuplot
 * to create an animation of the Ising model.
 *
 * @param TT The temperature.
 */
void animate_glauber(const double TT) {
    default_random_engine generator(time(NULL));

    const size_t NN_2 = NN * NN;  // NN squared
    double w_boltzmann[2];
    ising::boltzmann_weight_glauber(w_boltzmann, TT);

    bitset<NN_2> state;
    vector<size_t> ind_up, ind_down;
    ising::init_random_kawasaki(state, ind_up, ind_down, generator);

    // Dummy pointers
    int EE = 0, MM = 0;
    while (1) {
        ofstream o_stream;
        o_stream.open("animation.txt.tmp", ios::out | ios::trunc);
        ising::print_bitset_file(state, o_stream);
        o_stream.close();
        rename("animation.txt.tmp", "animation.txt");
        for (size_t tt = 0; tt < 100000; tt++) {
            ising::step_glauber(state, w_boltzmann, generator, EE, MM);
        }
    }
}

/**
 * Generate the data for an animated plot using Kawasaki-dynamics.
 *
 * This will create a file named `animation.txt` which can be used with gnuplot
 * to create an animation of the Ising model.
 *
 * @param TT The temperature.
 */
void animate_kawasaki(const double TT) {
    default_random_engine generator(time(NULL));

    const size_t NN_2 = NN * NN;  // NN squared

    double w_boltzmann[4];
    ising::boltzmann_weight_kawasaki(w_boltzmann, TT);

    bitset<NN_2> state;
    vector<size_t> ind_up, ind_down;
    ising::init_random_kawasaki(state, ind_up, ind_down, generator);

    // Dummy
    int EE = 0;
    while (1) {
        ofstream o_stream;
        o_stream.open("animation.txt.tmp", ios::out | ios::trunc);
        ising::print_bitset_file(state, o_stream);
        o_stream.close();
        rename("animation.txt.tmp", "animation.txt");
        for (size_t tt = 0; tt < 100000; tt++) {
            ising::step_kawasaki(state, ind_up, ind_down, w_boltzmann,
                                 generator, EE);
        }
    }
}

int main(int argc, char* argv[]) {
    if (cmd_option_exists(argv, argv + argc, "-h") ||
        cmd_option_exists(argv, argv + argc, "--help")) {
        print_usage();
        return 0;
    }

    // Parse args
    const char* sim_type = parse_cmd_option(argv, argv + argc, "-t", "g");
    const double TT = stod(parse_cmd_option(argv, argv + argc, "-T", "1.0"));

    switch (sim_type[0]) {
        case 'g':
            animate_glauber(TT);
            break;

        case 'k':
            animate_kawasaki(TT);
            break;

        default:
            print_usage();
    }

    return 0;
}
