/**
 * Module containing statistical tools to use for data generation and
 * evaluation.
 *
 */

#include "stat_tools.h"

using namespace std;

double bootstrap_specific_heat(const vector<int>& EE_measurements,
                               const vector<int>& EE_2_measurements,
                               const size_t N_resamplings, const double TT,
                               ising::RNG& generator) {
    vector<double> heat_capacities(N_resamplings);

    // random distribution between 0 and length of EE_measurements
    uniform_int_distribution<size_t> r_ind(0, EE_measurements.size() - 1);
    for (size_t ii = 0; ii < N_resamplings; ii++) {
        // resampled measurements
        vector<int> EE_resampled(EE_measurements.size());
        vector<int> EE_2_resampled(EE_measurements.size());

        // resample energies randomly (with replacement)
        for (size_t jj = 0; jj < EE_measurements.size(); jj++) {
            const size_t ind = r_ind(generator);
            EE_resampled[jj] = EE_measurements[ind];
            EE_2_resampled[jj] = EE_2_measurements[ind];
        }
        const double mean_EE = mean(EE_resampled);
        const double mean_EE_2 = mean(EE_2_resampled);
        heat_capacities[ii] = (mean_EE_2 - mean_EE * mean_EE);
    }
    return 1.0 / TT / TT / NN / NN * sqrt(variance(heat_capacities));
}

double bootstrap_susceptibility(const vector<int>& MM_measurements,
                                const vector<int>& MM_2_measurements,
                                const size_t N_resamplings, const double TT,
                                ising::RNG& generator) {
    vector<double> susceptibilities(N_resamplings);

    // random distribution between 0 and length of MM_measurements
    uniform_int_distribution<size_t> r_ind(0, MM_measurements.size() - 1);
    for (size_t ii = 0; ii < N_resamplings; ii++) {
        vector<int> MM_resampled(MM_measurements.size());
        vector<int> MM_2_resampled(MM_measurements.size());

        // resample magnetisations randomly (with replacement)
        for (size_t jj = 0; jj < MM_measurements.size(); jj++) {
            const size_t ind = r_ind(generator);
            MM_resampled[jj] = MM_measurements[ind];
            MM_2_resampled[jj] = MM_2_measurements[ind];
        }
        const double mean_MM = mean(MM_resampled);
        const double mean_MM_2 = mean(MM_2_resampled);
        susceptibilities[ii] = (mean_MM_2 - mean_MM * mean_MM);
    }
    return 1.0 / NN / NN / TT * sqrt(variance(susceptibilities));
}

