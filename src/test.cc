#include <assert.h>

#include <cstdint>
#include <ctime>
#include <iostream>
#include <random>

#include "ising_mc.h"
using namespace std;

typedef default_random_engine RNG;

void test_init() {
    ising::state_t state;

    cout << "Test all up:" << endl;
    ising::init_constant(state, true);
    int MM = ising::total_magnetisation(state);
    cout << "Total M: " << MM << endl;
    assert(MM == NN * NN);

    int EE = ising::total_e(state);
    cout << "Total E: " << EE << endl;
    assert(EE == -2 * NN * NN);

    cout << "Test all down:" << endl;
    ising::init_constant(state, false);
    MM = ising::total_magnetisation(state);
    cout << "Total M: " << MM << endl;
    assert(MM == -NN * NN);

    EE = ising::total_e(state);
    cout << "Total E: " << EE << endl;
    assert(EE == -2 * NN * NN);

    cout << endl << "All initialisation test passed" << endl << endl;
}

void test_glauber(RNG& generator) {
    const double TT = 1;

    double w_boltzmann[2];
    ising::boltzmann_weight_glauber(w_boltzmann, TT);

    ising::state_t state;

    ising::init_random(state, generator);
    // ising::init_constant(state, true);

    int E_updated = ising::total_e(state);
    int M_updated = ising::total_magnetisation(state);
    for (size_t tt = 0; tt < 1000000; tt++) {
        ising::step_glauber(state, w_boltzmann, generator, E_updated,
                            M_updated);
    }
    int E_end = ising::total_e(state);
    int M_end = ising::total_magnetisation(state);
    cout << "M: " << M_end << "\t" << M_updated << endl;
    cout << "E: " << E_end << "\t" << E_updated << endl;
    assert(M_end == M_updated);
    assert(E_end == E_updated);

    cout << endl;
    cout << "Glauber dynamics tests passed" << endl;
    cout << endl;
}

void test_kawasaki(RNG& generator) {
    const double TT = 1;

    double w_boltzmann[4];
    ising::boltzmann_weight_kawasaki(w_boltzmann, TT);

    ising::state_t state;
    vector<size_t> ind_up, ind_down;
    ising::init_random_kawasaki(state, ind_up, ind_down, generator);
    // ising::init_constant(state, true);
    cout << ind_up.size() << endl;

    int E_updated = ising::total_e(state);
    int M_init = ising::total_magnetisation(state);
    for (size_t tt = 0; tt < 1000000; tt++) {
        ising::step_kawasaki(state, ind_up, ind_down, w_boltzmann, generator,
                             E_updated);
    }
    int E_end = ising::total_e(state);
    int M_end = ising::total_magnetisation(state);
    cout << "M: " << M_end << "\t" << M_init << endl;
    cout << "E: " << E_end << "\t" << E_updated << endl;
    assert(M_end == M_init);
    assert(E_end == E_updated);

    cout << endl;
    cout << "Kawasaki dynamics tests passed" << endl;
}

int main() {
    default_random_engine generator(time(NULL));

    test_init();
    test_glauber(generator);
    test_kawasaki(generator);

    return 0;
}
