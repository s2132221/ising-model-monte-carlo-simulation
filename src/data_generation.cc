#include <algorithm>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <limits>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "ising_mc.h"
#include "stat_tools.h"

using namespace std;

/**
 * Return a non-linear distribution for non-uniform sampling.
 *
 * The resulting profile follows the form (TT - TT_crit)^alpha, i. e.
 * temperature sampling near the critical temperature becomes finer.
 *
 * @param TT The resulting non-linear parametrisation.
 * @param TT_min The lower bound of the parameter (temperature).
 * @param TT_max The upper bound of the parameter (temperature).
 * @param N_temperatures The number of points in the parametrisation.
 * @param TT_crit The critical parameter where sampling becomes
 *                particularly fine.
 * @param alpha The exponent governing the non-linearity.
 */
void variable_distribution(vector<double>& TT, const double TT_min,
                           const double TT_max, const size_t N_temperatures,
                           const double TT_crit, const double alpha = 2.0) {
    const size_t ii_crit = (size_t)floor(
        1.0 * N_temperatures * (TT_crit - TT_min) / (TT_max - TT_min));

    for (size_t ii = 0; ii < ii_crit; ii++) {
        // TT[ii] = (TT_min - TT_crit) * (ii - ii_crit) * (ii - ii_crit) +
        // TT_crit;
        TT[ii] = (TT_min - TT_crit) * pow(ii_crit, -alpha) *
                     pow(ii_crit - ii, alpha) +
                 TT_crit;
    }
    for (size_t ii = ii_crit; ii < N_temperatures; ii++) {
        TT[ii] = (TT_max - TT_crit) *
                     pow(N_temperatures - 1 - ii_crit, -alpha) *
                     pow(ii - ii_crit, alpha) +
                 TT_crit;
    }
}

/**
 * Parse a commandline argument.
 *
 * Return the value set for the flag `flag` in the commandline arguments or
 * a default parameter.
 *
 * @param begin The first index of the command line argument pointer.
 * @param end The last index of the command line argument pointer.
 * @param flag The flag to look for.
 * @param def The default value to return if `flag` was not specified.
 */
const char* parse_cmd_option(char** begin, char** end, const string& flag,
                             const char* def) {
    char** itr = std::find(begin, end, flag);
    if (itr != end && ++itr != end) {
        return *itr;
    }
    return def;
}

/**
 * Determine whether a specific flag exists in the command line arguments.
 *
 * @param begin The first index of the command line argument pointer.
 * @param end The last index of the command line argument pointer.
 */
bool cmd_option_exists(char** begin, char** end, const string& flag) {
    return std::find(begin, end, flag) != end;
}

void print_usage() {
    cout << "Usage: ./data_generation [-t (g|k)] [-f filename] [-Nm "
            "N_measurements] "
            "[-Tmin T_min] "
            "[-Tmax T_max] [-Tcrit T_crit] [-Nt N_temperatures] [-alpha "
            "exponent] [-s seed] "
            "[--adaptive]"
            "[--help] [-h]"
         << endl;
}

/**
 * Generate data files with Glauber dynamics.
 *
 * Generate a file with the name `filename` that columnwise contains:
 * 1. Temperature, 2. Mean energy, 4. Mean squared energy, 6. Mean absolute
 * magnetisation, 8. Mean magnetisation, 10. Mean squared magnetisation, 12.
 * Specific heat, 14. Susceptibility.
 * Each column is succeeded by the sampling error of the respective quantity.
 * For the specific heat and the susceptibility a bootstrapping approach was
 * pursued whereas the other errors are the standard deviation of the mean.
 *
 * @param TT_min The lowest temperature to sample
 * @param TT_max The highest temperature to sample
 * @param TT_crit An estimate of the critical temperature (for non-uniform
 *                sampling)
 * @param N_temperatures The number of temperature samples.
 * @param N_measurements The number of measurements per temperature
 * @param N_sweeps_equilibration The number of equilibration sweeps in the
 *                               beginning.
 * @param N_sweeps_in_between The number of sweeps between measurements.
 * @param N_bootstraps The number of bootstrapping draws to estimate the errors.
 * @param filename The output file name.
 * @param seed The random number generator seed
 * @param adaptive_sampling Specifies whether to adaptively control
 *                          N_sweeps_in_between.
 * @param alpha The exponent for non-uniform sampling.
 */
void generate_glauber_data(const double TT_min, const double TT_max,
                           const double TT_crit, const size_t N_temperatures,
                           const size_t N_measurements,
                           const size_t N_sweeps_equilibration,
                           size_t N_sweeps_in_between,
                           const size_t N_bootstraps, const char* filename,
                           const long seed, const bool adaptive_sampling,
                           const double alpha) {
    vector<double> temperatures(N_temperatures);
    variable_distribution(temperatures, TT_min, TT_max, N_temperatures, TT_crit,
                          alpha);

    ising::state_t state;
    // Initialise the system in the all-up state for faster equilibration.
    ising::init_constant(state, true);
    double w_boltzmann[2];

    // Define the seeded random number generator
    default_random_engine generator(seed);

    // Initial energy and magnetisation
    int EE = ising::total_e(state);
    int MM = ising::total_magnetisation(state);

    FILE* out_file;
    out_file = fopen(filename, "w");

    // Loop through temperatures
    for (double TT : temperatures) {
        printf("Initialise temperature %.2f\n", TT);
        if (adaptive_sampling) {
            N_sweeps_in_between = 100.0 / (pow(TT - TT_crit, 2.0) + 1);
            printf("Adapting equilibration steps to %zu\n",
                   N_sweeps_in_between);
        }

        vector<int> EE_measurements(N_measurements);
        vector<int> EE_2_measurements(N_measurements);  // Energy squared
        vector<int> MM_measurements(N_measurements);
        vector<int> MM_abs_measurements(N_measurements);
        vector<int> MM_2_measurements(N_measurements);

        ising::boltzmann_weight_glauber(w_boltzmann, TT);

        // 100 sweeps for equilibration
        ising::simulate_glauber(state, EE, MM, w_boltzmann, generator,
                                N_sweeps_equilibration);

        // record first measurement
        EE_measurements[0] = EE;
        EE_2_measurements[0] = EE * EE;
        MM_abs_measurements[0] = abs(MM);
        MM_measurements[0] = MM;
        MM_2_measurements[0] = MM * MM;

        // We already saved the first measurement (hence the -1)
        for (size_t ii = 0; ii < N_measurements - 1; ii++) {
            ising::simulate_glauber(state, EE, MM, w_boltzmann, generator,
                                    N_sweeps_in_between);

            // Perform measurements
            EE_measurements[ii + 1] = EE;
            EE_2_measurements[ii + 1] = EE * EE;
            MM_abs_measurements[ii + 1] = abs(MM);
            MM_measurements[ii + 1] = MM;
            MM_2_measurements[ii + 1] = MM * MM;
        }
        // Compute averages

        // cols 2:3
        const double mean_EE = mean(EE_measurements);
        const double delta_EE = sqrt(variance(EE_measurements, mean_EE));

        // cols 4:5
        const double mean_EE_2 = mean(EE_2_measurements);
        const double delta_EE_2 = sqrt(variance(EE_2_measurements, mean_EE_2));

        // cols 6:7
        const double mean_abs_MM = mean(MM_abs_measurements);
        const double delta_abs_MM =
            sqrt(variance(MM_abs_measurements, mean_abs_MM));

        // cols 8:9
        const double mean_MM = mean(MM_measurements);
        const double delta_MM = sqrt(variance(MM_measurements, mean_MM));

        // cols 10:11
        const double mean_MM_2 = mean(MM_2_measurements);
        const double delta_MM_2 = sqrt(variance(MM_2_measurements, mean_MM_2));

        // cols 12:13
        const double specific_heat =
            1.0 / NN / NN / TT / TT * (mean_EE_2 - mean_EE * mean_EE);
        const double delta_specific_heat = bootstrap_specific_heat(
            EE_measurements, EE_2_measurements, N_bootstraps, TT, generator);

        // cols 14:15
        const double chi = 1.0 / NN / NN / TT * (mean_MM_2 - mean_MM * mean_MM);
        const double delta_chi = bootstrap_susceptibility(
            MM_measurements, MM_2_measurements, N_bootstraps, TT, generator);

        // Write averages
        fprintf(out_file,
                "%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%."
                "10e\t%.10e\t%."
                "10e\t%.10e\t%."
                "10e\t%.10e\t%.10e\n",
                TT, mean_EE, delta_EE, mean_EE_2, delta_EE_2, mean_abs_MM,
                delta_abs_MM, mean_MM, delta_MM, mean_MM_2, delta_MM_2,
                specific_heat, delta_specific_heat, chi, delta_chi);
        fflush(out_file);
    }
    fclose(out_file);
}

/**
 * Generate data files with Kawasaki dynamics.
 *
 * Generate a file with the name `filename` that columnwise contains:
 * 1. Temperature, 2. Mean energy, 4. Mean squared energy, 6.
 * Specific heat.
 * Each column is succeeded by the sampling error of the respective quantity.
 * For the specific heat a bootstrapping approach was pursued whereas the other
 * errors are the standard deviation of the mean.
 *
 * @param TT_min The lowest temperature to sample
 * @param TT_max The highest temperature to sample
 * @param TT_crit An estimate of the critical temperature (for non-uniform
 *                sampling)
 * @param N_temperatures The number of temperature samples.
 * @param N_measurements The number of measurements per temperature
 * @param N_sweeps_equilibration The number of equilibration sweeps in the
 *                               beginning.
 * @param N_sweeps_in_between The number of sweeps between measurements.
 * @param N_bootstraps The number of bootstrapping draws to estimate the errors.
 * @param filename The output file name.
 * @param seed The random number generator seed
 * @param adaptive_sampling Specifies whether to adaptively control
 *                          N_sweeps_in_between.
 * @param alpha The exponent for non-uniform sampling.
 */
void generate_kawasaki_data(const double TT_min, const double TT_max,
                            const double TT_crit, const size_t N_temperatures,
                            const size_t N_measurements,
                            const size_t N_sweeps_equilibration,
                            size_t N_sweeps_in_between,
                            const size_t N_bootstraps, const char* filename,
                            const long seed, const bool adaptive_sampling,
                            const double alpha) {
    vector<double> temperatures(N_temperatures);
    variable_distribution(temperatures, TT_min, TT_max, N_temperatures, TT_crit,
                          alpha);

    // Define the seeded random number generator
    default_random_engine generator(seed);

    // Initialise the system randomly.
    ising::state_t state;
    vector<size_t> up_ind, down_ind;
    ising::init_random_kawasaki(state, up_ind, down_ind, generator);

    double w_boltzmann[4];
    int EE = ising::total_e(state);

    FILE* out_file;
    out_file = fopen(filename, "w");

    // Loop through temperatures backwards (start at high temperature to
    // shorten equilibration times).
    for (vector<double>::reverse_iterator temp_iter = temperatures.rbegin();
         temp_iter != temperatures.rend(); temp_iter++) {
        const double TT = *temp_iter;
        printf("Initialise temperature %.2f\n", TT);
        if (adaptive_sampling) {
            // N_sweeps_in_between = 10 + last_error * 8000;
            N_sweeps_in_between = 100.0 / (pow(TT - TT_crit, 2.0) + 1);
            printf("Adapting equilibration steps to %zu\n",
                   N_sweeps_in_between);
        }

        vector<int> EE_measurements(N_measurements);
        vector<int> EE_2_measurements(N_measurements);  // Energy squared

        ising::boltzmann_weight_kawasaki(w_boltzmann, TT);

        // 100 sweeps for equilibration
        ising::simulate_kawasaki(state, up_ind, down_ind, EE, w_boltzmann,
                                 generator, N_sweeps_equilibration);

        EE_measurements[0] = EE;
        EE_2_measurements[0] = EE * EE;

        // We already saved the first measurement (hence the -1)
        for (size_t ii = 0; ii < N_measurements - 1; ii++) {
            ising::simulate_kawasaki(state, up_ind, down_ind, EE, w_boltzmann,
                                     generator, N_sweeps_in_between);
            EE_measurements[ii + 1] = EE;
            EE_2_measurements[ii + 1] = EE * EE;
        }

        // cols 2:3
        const double mean_EE = mean(EE_measurements);
        const double delta_EE = sqrt(variance(EE_measurements, mean_EE));

        // cols 4:5
        const double mean_EE_2 = mean(EE_2_measurements);
        const double delta_EE_2 = sqrt(variance(EE_2_measurements, mean_EE_2));
        // cols 12:13
        const double specific_heat =
            1.0 / NN / NN / TT / TT * (mean_EE_2 - mean_EE * mean_EE);
        const double delta_specific_heat = bootstrap_specific_heat(
            EE_measurements, EE_2_measurements, N_bootstraps, TT, generator);

        fprintf(out_file, "%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\t%.10e\n",
                TT, mean_EE, delta_EE, mean_EE_2, delta_EE_2, specific_heat,
                delta_specific_heat);
        fflush(out_file);
    }
    fclose(out_file);
}

int main(int argc, char* argv[]) {
    if (cmd_option_exists(argv, argv + argc, "-h") ||
        cmd_option_exists(argv, argv + argc, "--help")) {
        print_usage();
        return 0;
    }

    // Parse command line options
    const char* filename =
        parse_cmd_option(argv, argv + argc, "-f", "glauber_data.txt");
    const size_t N_sweeps_equilibration = 100;
    const size_t N_measurements =
        stol(parse_cmd_option(argv, argv + argc, "-Nm", "1000"));
    const size_t N_bootstraps = N_measurements / 10;
    const size_t N_sweeps_in_between = 10;
    const double TT_min =
        stod(parse_cmd_option(argv, argv + argc, "-Tmin", "0.1"));
    const double TT_max =
        stod(parse_cmd_option(argv, argv + argc, "-Tmax", "5.0"));
    const double TT_crit =
        stod(parse_cmd_option(argv, argv + argc, "-Tcrit", "2.29"));
    const size_t N_temperatures =
        stod(parse_cmd_option(argv, argv + argc, "-Nt", "50"));
    const double alpha =
        stod(parse_cmd_option(argv, argv + argc, "-alpha", "2.0"));
    const long seed =
        stol(parse_cmd_option(argv, argv + argc, "-s", "123456789"));
    const bool adaptive_sampling =
        cmd_option_exists(argv, argv + argc, "--adaptive");

    const char* type = parse_cmd_option(argv, argv + argc, "-t", "g");

    switch (type[0]) {
        case 'g':
            generate_glauber_data(TT_min, TT_max, TT_crit, N_temperatures,
                                  N_measurements, N_sweeps_equilibration,
                                  N_sweeps_in_between, N_bootstraps, filename,
                                  seed, adaptive_sampling, alpha);
            break;
        case 'k':
            generate_kawasaki_data(TT_min, TT_max, TT_crit, N_temperatures,
                                   N_measurements, N_sweeps_equilibration,
                                   N_sweeps_in_between, N_bootstraps, filename,
                                   seed, adaptive_sampling, alpha);
            break;
        default:
            print_usage();
            break;
    }

    return 0;
}
