#include "ising_mc.h"

#include <bitset>
#include <cstddef>
#include <cstdint>

namespace ising {
typedef std::default_random_engine RNG;
typedef std::bitset<NN * NN> state_t;
/**
 * Positive modulo.
 *
 * @param ii:
 */
inline int mod(int ii, int nn) { return (nn + ii % nn) % nn; }

/**
 * Convert 2D indices into a 1D index
 *
 * @param xx: x-index
 * @param yy: y-index
 */
inline size_t ind(const size_t xx, const size_t yy) {
    return mod(NN * yy + xx, NN * NN);
}
/**
 * Convert a linear index to the x index
 *
 */
inline size_t y_ind(const size_t ii) { return ii / NN; }

inline size_t x_ind(const size_t ii) { return ii % NN; }

inline size_t up(const size_t ii) { return mod(ii - NN, NN * NN); }

inline size_t down(const size_t ii) { return mod(ii + NN, NN * NN); }

inline size_t left(const size_t ii) { return NN * y_ind(ii) + mod(ii - 1, NN); }

inline size_t right(const size_t ii) {
    return NN * y_ind(ii) + mod(ii + 1, NN);
}

void boltzmann_weight_glauber(double (&w_boltzmann)[2], const double TT) {
    w_boltzmann[0] = exp(-8.0 / TT);
    w_boltzmann[1] = exp(-4.0 / TT);
}

void boltzmann_weight_kawasaki(double (&w_boltzmann)[4], const double TT) {
    w_boltzmann[0] = exp(-32.0 / TT);
    w_boltzmann[1] = exp(-24.0 / TT);
    w_boltzmann[2] = exp(-16.0 / TT);
    w_boltzmann[3] = exp(-8.0 / TT);
}

void print_bitset_file(const state_t& state, std::ostream& os) {
    for (size_t yy = 0; yy < NN; yy++) {
        for (size_t xx = 0; xx < NN; xx++) {
            os << xx << "\t" << yy << "\t" << state.test(ind(xx, yy))
               << std::endl;
        }
        os << std::endl;
    }
}

void print_bitset(const state_t& state, std::ostream& os) {
    for (size_t yy = 0; yy < NN; yy++) {
        for (size_t xx = 0; xx < NN; xx++) {
            os << state.test(ind(xx, yy)) << "\t";
        }
        os << std::endl;
    }
}

void print_bitset(const state_t& state) { print_bitset(state, std::cout); }

/**
 * Return the energy difference if the spin with index ii were flipped.
 *
 * The energy difference is encoded as follows:
 * 000: +8J
 * 001: +4J
 * 010:  0J
 * 011: -4J
 * 100: -8J
 */
unsigned delta_e(const state_t& state, const size_t ii) {
    const bool spin = state.test(ii);

    // stop auto formatter from messing up perfectly fine code:
    // clang-format off
    const unsigned delta_e =   (spin ^ state.test(left (ii)))
                        + (spin ^ state.test(right(ii)))
                        + (spin ^ state.test(up   (ii)))
                        + (spin ^ state.test(down (ii)));
    // clang-format on
    return delta_e;
}

/**
 * Return the energy contribution of a single spin at site ii considering its
 * nearest neighbours.
 */
int local_e(const state_t& state, const size_t ii) {
    const bool spin = state.test(ii);

    // stop auto formatter from messing up perfectly fine code:
    // clang-format off
    const int  ee =   (spin ^ state.test(left (ii)))
                    + (spin ^ state.test(right(ii)))
                    + (spin ^ state.test(up   (ii)))
                    + (spin ^ state.test(down (ii)));
    // clang-format on
    return -2 + ee;
}

/**
 * Compute the total energy of the system assuming J=1.
 *
 * @param state The state of all spins in the system.
 */
int total_e(const state_t& state) {
    int E = 0;
    for (size_t ii = 0; ii < NN * NN; ii++) {
        E += local_e(state, ii);
    }
    return E;
}

/**
 * Compute the total magnetization of a given state.
 *
 * @param state The state of all spins in the system.
 */
int total_magnetisation(const state_t& state) {
    return 2 * (int)state.count() - NN * NN;
}

bool step_glauber(state_t& state, const double (&w_boltzmann)[2],
                  RNG& generator, int& EE, int& MM) {
    std::uniform_real_distribution<double> rand_uniform(0, 1);
    // choose spin at random
    const size_t ind = (size_t)(rand_uniform(generator) * NN * NN);
    const int dE = (int)delta_e(state, ind);
    switch (dE) {
        case 0:  // Energy difference of +8J
            if (rand_uniform(generator) < w_boltzmann[0]) {
                state.flip(ind);
                break;
            }
            return false;
        case 1:  // Energy difference of +4J
            if (rand_uniform(generator) < w_boltzmann[1]) {
                state.flip(ind);
                break;
            }
            return false;
        default:  // Energy difference <= 0J
            state.flip(ind);
            break;
    }
    // Spin was flipped
    MM += 4 * (int)state.test(ind) - 2;
    EE += -4 * dE + 8;
    return true;
}

bool step_kawasaki(state_t& state, std::vector<size_t>& ind_up,
                   std::vector<size_t>& ind_down,
                   const double (&w_boltzmann)[4], RNG& generator, int& EE) {
    std::uniform_real_distribution<double> rand_uniform(0, 1);
    // const size_t ii = (size_t)(rand_uniform(generator) * NN * NN);

    // // Look for an index jj where the spin is opposite to S_ii
    // size_t jj;
    // do {
    //     jj = (size_t)(rand_uniform(generator) * NN * NN);
    // } while (state.test(ii) == state.test(jj));

    const size_t i_up = (size_t)(rand_uniform(generator) * ind_up.size());
    const size_t ii = ind_up[i_up];

    const size_t i_down = (size_t)(rand_uniform(generator) * ind_down.size());
    const size_t jj = ind_down[i_down];
    // const size_t ii = 13, jj = 14;

    // generate spin flip mask
    // std::bitset<NN* NN> mask = (state_t(1) << (ii) | state_t(1) << (jj));

    // This results in the following:
    // dE =
    //
    // 0000:  16J
    // 0001:  12J
    // 0010:   8J
    // 0011:   4J
    // 0100:   0J
    // 0101:  -4J
    // 0110:  -8J
    // 0111: -12J
    // 1000: -16J
    //
    int dE = (int)(delta_e(state, ii) + delta_e(state, jj));

    // Correction for nearest neighbours
    dE -= (ii == left(jj) || ii == right(jj) || ii == up(jj) || ii == down(jj));
    if (dE < 4 && rand_uniform(generator) > w_boltzmann[dE]) {
        return false;
    }
    // state ^= mask;
    state.flip(ii);
    state.flip(jj);
    EE += -4 * dE + 16;

    // swap indices in provided vectors
    ind_down[i_down] = ii;
    ind_up[i_up] = jj;
    return true;
}

void sweep_glauber(ising::state_t& state, int& EE, int& MM,
                   const double (&w_boltzmann)[2], ising::RNG& generator) {
    for (size_t ii = 0; ii < NN * NN; ii++) {
        step_glauber(state, w_boltzmann, generator, EE, MM);
    }
}

void simulate_glauber(ising::state_t& state, int& EE, int& MM,
                      const double (&w_boltzmann)[2], ising::RNG& generator,
                      const size_t N_sweeps) {
    for (size_t ii = 0; ii < N_sweeps * NN * NN; ii++) {
        step_glauber(state, w_boltzmann, generator, EE, MM);
    }
}

void sweep_kawasaki(ising::state_t& state, std::vector<size_t>& ind_up,
                    std::vector<size_t>& ind_down, int& EE,
                    const double (&w_boltzmann)[4], ising::RNG& generator) {
    for (size_t ii = 0; ii < NN * NN; ii++) {
        step_kawasaki(state, ind_up, ind_down, w_boltzmann, generator, EE);
    }
}

void simulate_kawasaki(ising::state_t& state, std::vector<size_t>& ind_up,
                       std::vector<size_t>& ind_down, int& EE,
                       const double (&w_boltzmann)[4], ising::RNG& generator,
                       const size_t N_sweeps) {
    for (size_t ii = 0; ii < N_sweeps * NN * NN; ii++) {
        step_kawasaki(state, ind_up, ind_down, w_boltzmann, generator, EE);
    }
}

void init_random(state_t& state, RNG& generator) {
    std::uniform_real_distribution<double> rand_uniform(0, 1);
    for (size_t i = 0; i < NN * NN; i++) {
        state.set(i, rand_uniform(generator) < 0.5);
    }
}

void init_random_kawasaki(state_t& state, std::vector<size_t>& ind_up,
                          std::vector<size_t>& ind_down, RNG& generator) {
    std::uniform_real_distribution<double> rand_uniform(0, 1);
    for (size_t i = 0; i < NN * NN; i++) {
        const bool ss = rand_uniform(generator) < 0.5;
        state.set(i, ss);
        if (ss) {
            ind_up.push_back(i);
        } else {
            ind_down.push_back(i);
        }
    }
}

void init_constant(state_t& state, const bool up_down) {
    if (up_down) {
        state.set();
    } else {
        state.reset();
    }
}
}  // namespace ising
