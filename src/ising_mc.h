#include <vector>
#ifndef _ising_mc_h

#include <math.h>
#include <stdio.h>

#include <bitset>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <ios>
#include <iostream>
#include <ostream>
#include <random>

#ifndef NN
#define NN 50
#endif
namespace ising {
typedef std::default_random_engine RNG;
typedef std::bitset<NN * NN> state_t;

void print_bitset_file(const state_t& state, std::ostream& os);

void print_bitset(const state_t& state, std::ostream& os);

void print_bitset(const state_t& state);

void boltzmann_weight_glauber(double (&w_boltzmann)[2], const double TT);

void boltzmann_weight_kawasaki(double (&w_boltzmann)[4], const double TT);

unsigned delta_e(const state_t& state, const size_t ii);

int local_e(const state_t& state, const size_t ii);

void init_random(state_t& state, RNG& generator);

void init_random_kawasaki(state_t& state, std::vector<size_t>& ind_up, std::vector<size_t>& ind_down, RNG& generator);

void init_constant(state_t& state, const bool up_down = true);

int total_e(const state_t& state);

int total_magnetisation(const state_t& state);

bool step_glauber(state_t& state, const double (&w_boltzmann)[2],
                  RNG& generator, int& EE, int& MM);

bool step_kawasaki(state_t& state, std::vector<size_t>& ind_up,
                   std::vector<size_t>& ind_down,
                   const double (&w_boltzmann)[4], RNG& generator, int& EE);
void sweep_glauber(ising::state_t& state, int& EE, int& MM,
                   const double (&w_boltzmann)[2], ising::RNG& generator);
void simulate_glauber(ising::state_t& state, int& EE, int& MM,
                      const double (&w_boltzmann)[2], ising::RNG& generator,
                      const size_t N_sweeps);

void sweep_kawasaki(ising::state_t& state, std::vector<size_t>& ind_up,
                    std::vector<size_t>& ind_down, int& EE,
                    const double (&w_boltzmann)[4], ising::RNG& generator);
void simulate_kawasaki(ising::state_t& state, std::vector<size_t>& ind_up,
                       std::vector<size_t>& ind_down, int& EE,
                       const double (&w_boltzmann)[4], ising::RNG& generator,
                       const size_t N_sweeps);

}  // namespace ising
#define _ising_mc_h
#endif
