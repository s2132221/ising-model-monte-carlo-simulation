#ifndef _stat_tools_h

#include <random>
#include <vector>

#include "ising_mc.h"

template <typename T>
inline double mean(const std::vector<T>& data) {
    return 1.0 * accumulate(data.begin(), data.end(), 0.0) / data.size();
}

template <typename T>
double variance(const std::vector<T>& data, const double mean) {
    auto lambda_var = [&mean](double sum, const T& val) {
        return sum + ((val - mean) * (val - mean));
    };

    return 1.0 * accumulate(data.begin(), data.end(), 0.0, lambda_var) /
           (data.size() - 1);
}

template <typename T>
double variance(const std::vector<T>& data) {
    return variance(data, mean(data));
}

double bootstrap_specific_heat(const std::vector<int>& EE_measurements,
                               const std::vector<int>& EE_2_measurements,
                               const size_t N_resamplings, const double TT,
                               ising::RNG& generator);

double bootstrap_susceptibility(const std::vector<int>& MM_measurements,
                                const std::vector<int>& MM_2_measurements,
                                const size_t N_resamplings, const double TT,
                                ising::RNG& generator) ;

#define _stat_tools_h
#endif
