\documentclass[a4paper,12pt,parskip=full]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsfonts, amsmath, amssymb}
\usepackage{graphicx}
\usepackage[locale=US,per-mode=symbol,separate-uncertainty=true]{siunitx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{circuitikz}
\usepackage{siunitx}
\usepackage{xstring}
\usepackage{braket}
\usepackage{pdfpages}
\usepackage{wrapfig}
\usepackage{gnuplottex}
\usepackage{gnuplot-lua-tikz}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{black}
}
\usepackage[backend=biber]{biblatex}

\renewcommand{\tt}[1]{\text{#1}}
\renewcommand{\sc}{{\{s\}}}
\newcommand{\sci}{{\{s_i\}}}
\newcommand{\kb}{k_\tt{B}}
\newcommand{\avg}[1]{{\left < {#1} \right >}}
\newcommand{\abs}[1]{\left| #1 \right|}

\addbibresource{literature.bib}

\title{Markov Chain Monte Carlo simulation of the 2D Ising model}
\subtitle{Modelling and Visualisation in Physics\\
Semester 2 (2020/2021)\\
University of Edinburgh
}
\author{Leon Hillmann}
\date{\footnotesize{Submission: 04.02.2021}\\
\footnotesize{Professor Davide Marenduzzo}}


\begin{document}
\maketitle

\section{Introduction}
This project considers the Ising model of spins on a quadratic lattice. A
Markov Chain Monte Carlo algorithm is implemented, verified and used to
determine the system’s critical temperature without and with a conserved
order parameter.

The source code for this project is available in a git repository under
\url{https://git.ecdf.ed.ac.uk/s2132221/ising-model-monte-carlo-simulation}
(to be activated after the project submission deadline).

\section{Physical background}
The Ising model describes a regular lattice of spins $s_i = \pm1$ governed by
the Hamiltonian

\begin{equation}
  H(\left\{s\right\}) = -J \sum_{<i,j>}s_is_j - h \sum_i s_i, \label{eq:hamiltonian}
\end{equation}

where $<i,j>$ denotes the sum over nearest neighbours. In this project we
specifically consider the two dimensional Ising model without an external field
aht thus set $h=0$. Furthermore, we limit our analysis to the ferro-magnetic case
$J>0$, assume periodic boundary conditions and work with dimensionless units by
setting $J=\kb=1$. 

\subsection{Phases in the Ising model}
The Hamiltonian of the Ising model in (\ref{eq:hamiltonian}) for $J>0$ and $H=0$
benefits ordered configurations where all spins face either up or down. These
correspond to two of the $2^N$ possible system states, so at finite temperatures
entropy will push the system out of the fully ordered configurations. At low
temperatures thermal fluctuations are small enough for the system to remain
mostly ordered, which we will call the ordered phase, but above a critical
temperature $T_\tt{crit}$ fluctuations dominate and the system enters
the unordered phase. To estimate $T_\tt{crit}$ and study the phase transition
we consider the energy

\begin{equation}
  \avg{E} = - \frac{\partial \ln Z}{\partial \beta} = \kb T^2 \frac{\partial \ln Z} {\partial T}
\end{equation}

and the magnetisation

\begin{equation}
  M = \sum_i s_i \quad \Rightarrow \quad \avg{M} = \left.\frac{\partial \ln Z}{\partial h}\right |_{h=0}. \label{eq:magnetisation}
\end{equation}

We also denote the magnetisation per spin as $m = M/N$. Close to the phase
transition the system is particularly sensitive to a change in the external
field which we will later exploit to find the critical temperature. The response
of the magnetisation due to $h$ is given by the derivative of
(\ref{eq:magnetisation}) by the external field which defines the magnetic
susceptibility

\begin{equation}
  \chi = \left. \frac{\partial m}{\partial h}\right |_{h = 0} = \frac{1}{\beta N} \left. \frac{\partial^2 \ln Z}{\partial h^2} \right |_{h = 0} =  \frac{1}{N\kb T}(\avg{M^2} - \avg{M}^2),
\end{equation}

where we normalised by the number of spins and $\beta = (\kb T)^{-1}$. While the susceptibility is a good
indicator to use in Glauber dynamics, the magnetisation is conserved under the
Kawasaki algorithm. Instead of the susceptibility we therefore examine the
scaled specific heat $C$ which is defined by the temperature derivative of the
average energy per spin

\begin{align}
  % \avg{E} &= -\frac{\partial \ln Z}{\partial \beta} = \kb T^2 \frac{\partial \ln Z}{\partial T} \\
  C &= \frac{1}{N}\frac{\partial \avg{E}}{\partial T} = \frac{1}{N\kb T^2}(\avg{E^2} - \avg{E}^2).
\end{align}


\subsection{Monte Carlo sampling}
Generally, thermodynamic averages of an observable $O$ for such a spin system can be written as
\begin{equation}
  \avg{O} = \sum_{\{s\}} O(\{s\}) p_\tt{eq}(\{s\}) \label{eq:thermodynamic_avg}
\end{equation}
with
\begin{equation}
  p_\tt{eq}(\{s\}) = \frac{1}{Z} \sum_{\{s\}} \exp{-\left\{\frac{H(\{s\})}{\kb T}\right\}}.
\end{equation}
In systems with a large number of spins $N$, the number of possible
configuration grows as $2^N$ which quickly exceeds the limits even of modern
hardware. We will therefore simulate the system using a Markov chain Monte Carlo
simulation which will sample the states with high weights with a higher
probability hence yielding reasonable estimates for thermodynamic averages. In
the course of this project we will explore two different dynamics simulations
which are explained in the following.

\subsubsection*{Glauber dynamics}
The single spin flip algorithm for Glauber dynamics consists of choosing a
random spin to flip and accepting the change with

\begin{equation}
  \alpha = \min\left(1, \exp\left\{\frac{\Delta H}{\kb T}\right\}\right), \label{eq:acceptance_glauber}
\end{equation}

where $\Delta H$ is the energy difference caused by the flip. Practically this
means that we only need to compute the total energy once and are subsequently
able to update it depending on the sum of the flipped spin's neighbours:

\begin{equation}
  \Delta H_i = -2s_i \sum_{j \in \tt{neighbours}} s_j \quad \Rightarrow \quad \Delta H_i \in \{-8, -4, 0, 4, 8\}, \label{eq:single_flip_delta_e}
\end{equation}
with $s_i$ denoting the original state of the spin proposed to be flipped and the
sum running over its neighbours.
The factor of two in the front is justified by the fact that flipping a spin
changes its contribution by 2 (going from -1 to +1 or vice versa).
Since there are only four neighbours for each spin, there are five possible energy
differences in total for three of which $\alpha = 1$. Therefore the
exponential in (\ref{eq:acceptance_glauber}) only has to be computed for
$\Delta H = 4$ and $\Delta H = 8$ in the program, which can be done once in advance.

\subsubsection*{Kawasaki dynamics}
Contrary to the Glauber dynamics, the Kawasaki dynamics conserves the total
magnetisation in the model by a slight adjustment to the spin flip algorithm.
Rather than proposing a single spin, we instead choose two opposite facing ones
and attempt to flip them simultaneously, essentially swapping their places.
This obviously conserves the total number of upwards and downwards facing spins,
keeping the magnetisation constant. The flips are accepted with the same rate as
in (\ref{eq:acceptance_glauber}) with the energy difference being the sum of
two single spin flips and a correction term:

\begin{equation}
  \Delta H = \Delta H_i + \Delta H_j + \Delta H_{i,j}^{(n.n.)}.
\end{equation}

$\Delta H_i$ refers to the single spin flip energy difference according to
(\ref{eq:single_flip_delta_e}) and $\Delta H^{(n.n.)}_{i,j}$ is the correction
applied if $i$ and $j$ are nearest neighbours

\begin{equation}
  \frac{\Delta H_{i,j}^{(n.n.)}}{\kb T} = \begin{cases}
    4  & \text{if $i, j$ nearest neighbours} \\
    0 & \mathrm{otherwise.}
  \end{cases}
\end{equation}

The correction of four arises because if $i$ and $j$ are nearest neighbours,
this eliminates the energy change for one of each spin's neighbouring
connections. By swapping opposite faced nearest neighbours the energy of that
particular nearest neighbour connection does not change,
(\ref{eq:single_flip_delta_e}) would however result in a non zero contribution
and even double count the connection which is compensated by the correction
term. Similar to the Glauber dynamics, there are only very few possible energy
differences, namely $\Delta H = \pm 16,\pm 12, \pm 8, \pm 4$ and $\Delta H = 0$
which means that we can again compute $\alpha$ for the relevant cases once in
advance.

\subsubsection{Averages in Monte Carlo simulations}
Applying either of these update methods to the system, a thermodynamic average
as in (\ref{eq:thermodynamic_avg}) can then be computed as the average
of multiple measurements during the simulation

\begin{equation}
  \avg{O} \approx \frac{1}{N_\tt{m}} \sum_i^{N_\tt{m}} O_i \label{eq:avg_mc}
\end{equation}

where $O_i$ are $N_\tt{m}$ measurements of the observable at different times
Similarly we can estimate the error of this average by the standard
deviation or the standard error of the mean.

\section{Implementation}
\subsection{State representation}
The programs to produce the data files simulate the described Glauber and
Kawasaki dynamics and were implemented in \texttt{c++}. In order to efficiently
store the current state of the system
in memory, the \texttt{bitset<N>} template was used, where \texttt{N} is the
total number of spins. The \texttt{bitset} data structure allows for the
representation of spins by individual bits where $0$ was assigned to a down
facing and $1$ to an up facing spin. Operations like querying or flipping a
single bit on this are well optimised and fast, however this choice comes with
the drawback that the number of spins has to be known at compile time which
necessitates the rebuild of the entire simulation code if we want to run
a simulation for a different $N$.


\subsection{Energy computation}
Representing spins as single bits also brings the advantage of being able to
write the negative product over nearest neighbours in (\ref{eq:hamiltonian})
as sum of \texttt{xor} operations as shown in listing~\ref{lst:local_e},
because neighbouring spins with opposite signs lead to an energy penalty for $J
> 0$.

\begin{lstlisting}[language=c++,caption={\textbf{Function to compute the energy of the nearest neighbours if a spin at index $ii$}},label={lst:local_e}]
int local_e(const state_t& state, const size_t ii) {
    const bool spin = state.test(ii);

    const int  ee =   (spin ^ state.test(left (ii)))
                    + (spin ^ state.test(right(ii)))
                    + (spin ^ state.test(up   (ii)))
                    + (spin ^ state.test(down (ii)));

    return -2 + ee;
}
\end{lstlisting}

While the proposal of a random spin in Glauber dynamics is straightforward, the
Kawasaki algorithm requires the choice of two opposite facing spins. Therefore
two lists of constant length which store the indices of up and down facing spins
respectively are initialised in the beginning and updated every step, such that
proposing a spin pair just involves choosing a random index in each list. This
strategy should be superior to choosing spins randomly and discard the proposal
if spins are parallel, especially for systems with high absolute magnetisation
as in that case it is very likely to propose parallel spins.

\subsection{Update}
Both, the Glauber and Kawasaki algorithm require to accept spin flips with the
rate $\alpha$ defined in (\ref{eq:acceptance_glauber}) which as stated earlier
can be computed once in advance for all cases. The energy difference between
two configurations can then be associated with the nearest neighbours of the
proposed spin which is calculated as in listing~\ref{lst:local_e} as in
~\cite{hassani2018parallelization}. During this step, the total energy
and the total magnetisation are changed according to the contribution of the spin flip.
These updates are verified in \texttt{test.cc} by computing the
energy and magnetisation explicitly and comparing both with the values updated
throughout the simulation.

\subsection{Measurements}
A Monte-Carlo sweep is defined as the number of steps it takes on average to
flip each spins, so a sweep consists of $N$ single steps. For the
computation of thermodynamic averages as in (\ref{eq:avg_mc}) we consistently
recorded $N_\tt{m} = 1000$ measurements for each temperature. Also, because we
were especially interested in the behaviour close to criticality, temperatures were
spaced non-uniformly with more samples in the vicinity of $T_\tt{crit}$. In the Glauber
dynamics measurements, the temperature samples in fig.~\ref{fg:non-uniform-temp}
were taken which roughly resemble two half-parabola. A similar profile was
also used for Kawasaki with the critical temperature guess changed accordingly.

\begin{figure}[htpb]
  \centering
  \begin{gnuplot}[terminal=pdf,terminaloptions={color size 12cm,8cm}]
    set xlabel "i"
    set ylabel "T"
    plot "../data/glauber_data.txt" u 1 notitle,2.29 title "T_c_r_i_t" lt 2 dt 2 lw 2
  \end{gnuplot}
  \caption{\textbf{Non-uniform distribution of temperature points as used for
  the Glauber dynamics.} Spacing in between samples decreases in the vicinity of
  the critical point.}
  \label{fg:non-uniform-temp}
\end{figure}

\subsubsection*{Correlations}
Equation (\ref{eq:avg_mc}) only delivers correct thermodynamic averages if the
measurements are uncorrelated. Close to the critical point however, the
correlation time diverges. In order to reduce the error created by correlated
samples we increased the time between measurements dynamically such that close
to the critical point we equilibrated for approximately 110 sweeps after each
measurement whereas infinitely war away we would only wait for 10 sweeps. The
updated equilibration time followed the profile

\begin{equation}
  T_\tt{eq} = 10  + \frac{100}{(T - T_\tt{crit})^2 + 1},
\end{equation}
which was empirically chosen by running several simulations and trying to
minimise the scatter close to the critical temperature. During testing this
profile turned out to yield the best results.

\subsubsection*{Bootstrapping}
While the standard deviation can be taken as an error estimate for $E$ and
$M$, the errors of the specific heat and susceptibility are more complicated
to calculate directly. Therefore their errors were obtained with a bootstrapping
approach where the measurements of $C$ and $\chi$ were resampled randomly with
replacement 100 times per temperature. The standard deviation of these
resamplings could then be computed as the approximate error of the
measurements.


\section{Results and discussion}

\subsection{Qualitative assessment and animations}
On order to get an overview of the behaviour of the Ising model at different
temperatures and under the two dynamics we first consider frames from an
animation of the system. Animations can be created with the supplied code
by running the command
\begin{lstlisting}[language=bash]
  make animation N=100 T=2.5 type=g
\end{lstlisting}

\begin{figure}[b!]
  \centering
  \begin{subfigure}{.3\textwidth}
  \begin{center}

   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_g_T_1.5.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Glauber, T=1.5}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
  \begin{center}

   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_g_T_crit.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Glauber, $T\approx2.29$}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
  \begin{center}

   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_g_T_4.5.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Glauber, $T=4.5$}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
  \begin{center}
   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_k_T_1.5.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Kawasaki, $T=1.5$}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
  \begin{center}
   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_k_T_crit.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Kawasaki, $T\approx2.29$}
  \end{subfigure}
  \begin{subfigure}{.3\textwidth}
  \begin{center}
   \begin{gnuplot}[terminal=pdf,terminaloptions={color size 5cm,4cm}]
     set cbrange[0:1]
     set palette defined (0 "black", 1 "white")
     unset xtics
     unset ytics
     unset colorbox
     plot[0:100][0:100] "../data/still_frame_k_T_4.5.txt" with image notitle
  \end{gnuplot}
  \end{center}
  \caption{Kawasaki, $T=4.5$}
  \end{subfigure}
  \caption{\textbf{Plots of the spin field for different temperatures and
  dynamics.} A black pixel corresponds to a downwards facing spin and a white
  pixel resembles an upwards facing one. All simulations were ran with a $100\times100$ grid.}
  \label{fig:ising_animation_frames}
\end{figure}

where \texttt{type} can be either \texttt{g} for Glauber or \texttt{k} for
Kawasaki dynamics and \texttt{N} and \texttt{T} define the width of the square
lattice and the temperature respectively. Fig.~\ref{fig:ising_animation_frames}
contains six frames taken from animations for various temperatures and the two
Metropolis algorithms. For low temperatures, the system in Glauber dynamics
tends to heavily favour either the all-spins-up or all-spins-down configuration
(with some noise), as it is the most energy efficient. After random
initialisation, large droplets form early in the process and shrink over time
because of surface tension until the system is uniform apart from small thermal
fluctuations. Due to the conservation of total magnetisation this is not
possible for the Kawasaki dynamics, therefore the system enters a ``slab''
configuration, a state that is meta-stable in Glauber dynamics and thus
sometimes is also observed with that algorithm. A quick analysis for $M/N\approx
0.5$ shows such a slab minimising the surface tension which in general has to
increase with the surface area or, in the 2d case, the domain wall length.
Intuitively one would argue that a circular droplet would be the ideal shape
with presence of surface tension. Because of the periodic boundary conditions
however, the domain boarder length for a slab is roughly $2L$ whereas a
circular drop of the same volume would have a circumference of $\sqrt{2\pi}L
\approx 2.5L$ which is larger~\cite{mueller19}.

Near the critical temperature of the infinite 2D Ising model of
$T\approx2.29 $~\cite{mueller19}, we can observe
scale invariant behaviour for the Glauber simulations which means that there
are islands of either spin state at any scale~\cite{krueger20}. In other words, the system will
look the same no matter how far we zoom in or out, which also means that
correlations diverge as we will see below. The animation of the
Kawasaki dynamics on the other hand does not change qualitatively for the
aforementioned critical temperature; its critical temperature will later be
confirmed to be around $T\approx4.5$. At this point
(fig.~\ref{fig:ising_animation_frames}~(f)), we can observe scale invariance
with 
the Kawasaki dynamics while the Glauber dynamics is already completely governed
by thermal fluctuations (fig.~\ref{fig:ising_animation_frames} (c)).



\subsection{Phase transitions}

\subsubsection*{Glauber dynamics}
Fig.~\ref{fig:energy_glauber} shows the energy and magnetisation per spin for
various temperatures under the Glauber dynamics. As expected, the energy rises
with the temperature due to the increase of thermal fluctuations.
Similarly, the absolute magnetisation per spin asymptotically goes to one as
the temperature goes to zero, indicating the ordered phase. Close to
$T\approx2.29 $ the magnetisation per spin drops rapidly and approaches zero for
higher temperatures corresponding to the disordered phase. The magnetisation as
an order parameter seems to change continuously but very rapidly close to $T_\tt{crit}$ hinting on a
first-order phase transition and a divergence of its first derivative by the
external field, the susceptibility. Considering the energy plot we can clearly
also expect a maximum in its derivative by temperature at the critical point, a
divergence is however not immediately obvious.

\begin{figure}[htpb]
  \centering
  \begin{subfigure}[]{.49\textwidth}
    \begin{center}
      \begin{gnuplot}[terminal=pdf,terminaloptions={color size 8.5cm,7cm}]
        set xlabel "T"
        set ylabel "E/N"
        set title "Energy per spin"
        plot "../data/glauber_data.txt" u 1:($2/2500):($3/2500) w errorlines notitle
      \end{gnuplot}
    \end{center}
  \end{subfigure}
  \begin{subfigure}[]{.49\textwidth}
    \begin{center}
      \begin{gnuplot}[terminal=pdf,terminaloptions={color size 8.5cm,7cm}]
        set xlabel "T"
        set ylabel "|M|/N"
        set title "Magnetisation per spin"
        plot "../data/glauber_data.txt" u 1:($6/2500):($7/2500) w errorlines notitle
      \end{gnuplot}
    \end{center}
  \end{subfigure}
  \caption{\textbf{Total energy (left) and absolute magnetisation (right) per spin for the
  Glauber dynamics.} The error bars in both plots represent the standard deviation for the respective quantity. The data series consists of 50 non-uniformly spread temperature points.}%
  \label{fig:energy_glauber}
\end{figure}


\subsection*{Kawasaki dynamics}
Just as in the Glauber case, fig.~\ref{fig:energy_kawasaki} contains a plot
of the energy per spin over temperature for a system governed by Kawasaki
dynamics. Since in this model the total magnetisation is conserved, we omitted
a plot of the magnetisation over temperature because it would simply be constant
in for all measurements (in fact, this is one of the tests implemented in
\texttt{test.cc} in the project source code). As before we may identify a
critical temperature at which the scaled heat capacity, i. e. the first
derivative of the energy per spin by temperature, is maximal.

\begin{figure}[htpb]
  \centering
  \begin{gnuplot}[terminal=pdf,terminaloptions={color size 12cm,8cm}]
        set xlabel "T"
        set ylabel "E/N"
        plot "../data/kawasaki_data.txt" u 1:($2/2500):($3/2500) w errorlines notitle
  \end{gnuplot}
  \caption{\textbf{Energy per spin in Kawasaki dynamics.} Again, samples were
  taken for 50 different temperatures that were closer together in the vicinity
  of our estimate for the critical temperature.}%
  \label{fig:energy_kawasaki}
\end{figure}


\subsection{Critical temperatures}
From the plots of the specific energy and absolute magnetisation above it is
difficult to identify the exact critical temperature for the two dynamics
models. Therefore we will examine their first derivatives, namely the scaled
heat capacity and the magnetic susceptibility in the following section to
obtain an estimate of $T_\tt{crit}$.

\subsubsection*{Glauber dynamics}
In the non-conserved order parameter simulation we were able to exploit both,
the divergence of the scaled heat capacity and of the magnetic susceptibility
to find the critical temperature. Plotting those quantities over temperature
as in fig.~\ref{fig:energy_glauber}, we can estimate the critical
temperature to be around $T_\tt{crit}\approx2.29$ which is close to the
critical temperature of the infinite 2D Ising model found in literature of
$T_\tt{crit,theo} \approx 2.269 $~\cite{mueller19}. The discrepancy arises
because the theoretical prediction holds for an infinite lattice while the
grid in the simulation is finite.

\begin{figure}[htpb]
  \centering
  \begin{subfigure}{.49\textwidth}
    \begin{center}
      \begin{gnuplot}[terminal=pdf,terminaloptions={color size 8.5cm,7cm}]
        set xlabel "T"
        set ylabel "C"
        set title "Scaled specific heat"
        plot "../data/glauber_data.txt" u 1:12:13 w errorlines notitle
      \end{gnuplot}
    \end{center}
  \end{subfigure}
  \begin{subfigure}[]{.49\textwidth}
    \begin{center}
      \begin{gnuplot}[terminal=pdf,terminaloptions={color size 8.5cm,7cm}]
        # load "./plot_susceptibility.gp"
        set xlabel "T"
        set ylabel "Chi"
        set title "Scaled susceptibility"
        plot "../data/glauber_data.txt" u 1:14:15 w errorlines notitle
      \end{gnuplot}
    \end{center}
  \end{subfigure}
  
  
  \caption{\textbf{Scaled specific heat (left) and susceptibility (right) for the
  Glauber dynamics simulation.} Both plots show a distinctive peak at $T\approx
  2.29$ for a computation on a $50\times50$ grid.}%
  \label{fig:specific_heat_glauber}
\end{figure}

\subsubsection*{Kawasaki dynamics}
In the case of a conserved order parameter, we were able to proceed exactly as
before. Since the total magnetisation does not change however,
fig.~\ref{fig:specific_heat_kawasaki} only shows a plot of the specific heat
over temperature which has its maximum around $T_\tt{crit}\approx4.5$, so
nearly double the critical temperature of the Glauber system. The
shift can be explained by the conservation of total magnetisation
introducing a Lagrange multiplier as additional term into the minimisation of
the free energy, changing it into a different effective free
energy~\cite{davide2020}. The observed peak is also less steep than the one
plotted for Glauber dynamics which agrees with the slope of the heat capacity in
fig~\ref{fig:energy_kawasaki} not becoming infinite.

\begin{figure}[htpb]
  \centering
  \begin{gnuplot}[terminal=pdf,terminaloptions={color size 12cm,8cm}]
    # load "./plot_specific_heat_kawasaki.gp"
    set xlabel "T"
    set ylabel "C"
    plot "../data/kawasaki_data.txt" u 1:6:7 w errorlines notitle
\end{gnuplot}
  \caption{\textbf{Specific heat in simulation with Kawasaki dynamics.} The peak
  corresponds to a temperature of roughly 4.5, it is however less pronounced
  than in fig.~\ref{fig:specific_heat_glauber}.}%
  \label{fig:specific_heat_kawasaki}
\end{figure}

\section{Summary}
By considering the 2D Ising model in a Markov Chain Monte Carlo simulation it
was possible to study the system's phase transition and critical point with and
without conserved order parameter. In particular the critical temperature in
Kawasaki dynamics was estimated to be $T_\tt{crit} \approx 4.5$ and
$T_\tt{crit}\approx 2.29 $ for the Glauber algorithm.


\printbibliography
\end{document}
