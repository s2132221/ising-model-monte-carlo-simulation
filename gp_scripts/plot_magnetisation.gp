set xlabel "T"
set ylabel "M"
set title "Glauber dynamics"
p "data/glauber_data.txt" u 1:6:7 w errorlines notitle pt 7 ps 0.8
