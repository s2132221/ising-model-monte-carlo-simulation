set xlabel "T"
set ylabel "E"
set title "Glauber dynamics"
p "data/glauber_data.txt" u 1:2:3 w errorlines notitle pt 7 ps 0.8
pause mouse keypress

set xlabel "T"
set ylabel "E"
set title "Kawasaki dynamics"
p "data/kawasaki_data.txt" u 1:2:3 w errorlines notitle pt 7 ps 0.8
pause mouse keypress
