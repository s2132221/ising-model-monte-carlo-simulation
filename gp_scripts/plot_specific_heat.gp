set xlabel "T"
set ylabel "C"
set title "Glauber dynamics"
plot "data/glauber_data.txt" u 1:12:13 title "specific heat" w errorlines pt 7 ps 0.8
pause mouse keypress

set title "Kawasaki dynamics"
plot "data/kawasaki_data.txt" u 1:6:7 title "specific heat" w errorlines pt 7 ps 0.8
pause mouse keypress
